import React from "react";

import "./App.css";
import { Ivideon } from "./componnets/ivideon";

function App() {
  return (
    <div className="App">
      <Ivideon />
    </div>
  );
}

export default App;
