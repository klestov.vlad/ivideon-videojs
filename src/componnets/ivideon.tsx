import React, { useEffect, useRef } from "react";
import VideoJS from "./VideoJS";

export const Ivideon: React.FC = () => {
  const [videoUrl, setVideoUrl] = React.useState<string | null>(null);

  const videoJsOptions = {
    autoplay: true,
    controls: true,
    responsive: true,
    fluid: true,
    sources: [
      {
        src: videoUrl,
        type: "video/mp4",
      },
    ],
  };

  useEffect(() => {
    const fetchStream = async () => {
      const streamUrl =
        "https://openapi-alpha-eu01.ivideon.com/cameras/100-DHbqCphF2VMsdMzFtvyfFR:0/live_stream";
      const queryParams = new URLSearchParams({
        format: "hls",
        q: "2",
        expect: "body",
        streams: "both",
        video_codecs: "h264",
        audio_codecs: "aac",
      });
      const token =
        "Bearer xtok-100U37-1bd56e2ad6a549479910ff4efffeb58e-mnZgdlop1aBIuHDe";

      try {
        const response = await fetch(`${streamUrl}?${queryParams.toString()}`, {
          headers: {
            Authorization: token,
          },
          redirect: "follow",
        });

        if (response.ok) {
          const streamResponse = await response.json();
          const videoUrl = streamResponse.result.url;
          setVideoUrl(videoUrl);
        } else {
          console.error(
            "Failed to fetch stream:",
            response.status,
            response.statusText
          );
        }
      } catch (error) {
        console.error("Error while fetching stream:", error);
      }
    };

    fetchStream();
  }, []);

  return videoUrl ? <VideoJS options={videoJsOptions} /> : <p>loading...</p>;
};
